module com.xsx.nb235 {
    requires javafx.controls;
    requires javafx.fxml;
            
                            
    opens com.xsx.nb235 to javafx.fxml;
    exports com.xsx.nb235;
    exports com.xsx.nb235.controller;
    opens com.xsx.nb235.controller to javafx.fxml;
    exports com.xsx.nb235.service;
    opens com.xsx.nb235.service to javafx.fxml;
}