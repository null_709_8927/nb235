package com.xsx.nb235.service;

import com.xsx.nb235.entity.Card;
import com.xsx.nb235.entity.Player;
import com.xsx.nb235.entity.Shape;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;

public class PokerGame {

    public static final List<Card> ALL_cards = new ArrayList<>();

    public static Map<Integer,Integer> playerScore = new LinkedHashMap();

    //给玩家发的牌
    public static Map<Integer,List<Card>> cardsMap = new LinkedHashMap();
    //玩家牌排序
    public static ConcurrentSkipListMap<Integer,int[]> playerCardMap = new ConcurrentSkipListMap();

    static{
        int[] numbers= {1,2,3,4,5,6,7,8,9,10,11,12,13};
        for(int n:numbers){
            for(Shape shape:Shape.values()){
                Card card = new Card(n,shape,PokerGame.class.getResource(shape.name().toLowerCase()+n+".jpg").toExternalForm());
                ALL_cards.add(card);
            }
        }
        //加入大小王
        // Collections.addAll(ALL_cards,new Card(14,Shape.HEART,"111"));
    }

    public static void shufflePoker(){
        Collections.shuffle(ALL_cards);
    }

    public static Map dealCards(ConcurrentSkipListMap<Integer, Player> cslm){
        cardsMap.clear();
        playerCardMap.clear();
        shufflePoker();
        int carno = 0;
        System.out.println("疯狂的发牌"+cslm.size());
        for(int uid:cslm.keySet()){
            ArrayList subcars = new ArrayList();
            for(int i=0;i<6;i++){
                subcars.add(ALL_cards.get(carno));
                carno++;
            }
            cardsMap.put(uid,subcars);
        }
        for(Map.Entry<Integer,List<Card>> e:cardsMap.entrySet()){
            System.out.println(e.getKey()+"="+e.getValue());
        }
        return cardsMap;
    }

    public static Map[] judge(){
        Map<Integer,Double> no1 = new HashMap<>();
        Map<Integer,Double> no2 = new HashMap<>();
        Map<Integer,Double> no3 = new HashMap<>();
        System.out.println("判决计数"+cardsMap.size());
        for(int s:cardsMap.keySet()){
            List<Card> list = cardsMap.get(s);
            list = resort(list,playerCardMap.get(s));
            cardsMap.put(s,list);
            //第一轮比较
            Card card0= list.get(0);
            no1.put(s,card0.getNumber());
            //第二轮比较
            Card card1= list.get(1);
            Card card2= list.get(2);
            Card [] ca = {card1,card2};
            no2.put(s,no2match(ca));
            //第三轮比较
            Card card3= list.get(3);
            Card card4= list.get(4);
            Card card5= list.get(5);
            List<Card> no3List = new ArrayList<>();
            no3List.add(card3);
            no3List.add(card4);
            no3List.add(card5);
            no3.put(s,no3match(no3List));
        }
        return new Map[]{no1,no2,no3};
    }

    public static Map<String,Integer> CalculateRanking(Map<Integer,Double> map){
        double highestRank = Collections.max(map.values());
        double lowestRank = Collections.min(map.values());
        HashMap<String,Integer> statiMap= new HashMap<>();
        if(highestRank>=600000&&lowestRank!=0) {
        	for (Map.Entry<Integer, Double> entry : map.entrySet()) {
                if (entry.getValue() == highestRank) {
                    statiMap.put("highest",entry.getKey());
                    statiMap.put("boom",entry.getKey());
                }
                if(entry.getValue() == lowestRank) {
                	statiMap.put("lowest",entry.getKey());
                }
            }
        }else if(highestRank>=600000&&lowestRank==0) {
        	for (Map.Entry<Integer, Double> entry : map.entrySet()) {
                if (entry.getValue() == 0) {
                    statiMap.put("highest",entry.getKey());
                    statiMap.put("boom",entry.getKey());
                }
                if(entry.getValue() == highestRank) {
                	statiMap.put("lowest",entry.getKey());
                }
            }
        }else if(highestRank==lowestRank){
        	
        }else {
        	for (Map.Entry<Integer, Double> entry : map.entrySet()) {
                if (entry.getValue() == highestRank) {
                    statiMap.put("highest",entry.getKey());
                }
                if(entry.getValue() == lowestRank) {
                	statiMap.put("lowest",entry.getKey());
                }
            }
        }
        return statiMap;
    }

    public static void statisticalScore(Map<String,Integer> map,int num){
        List<Integer> lowestList = new ArrayList<>();
        List<Integer> highestList = new ArrayList<>();
        List<Integer> boomList = new ArrayList<>();
        for(Map.Entry<String,Integer> e:map.entrySet()){
            if(e.getKey().equals("lowest")){
                lowestList.add(e.getValue());
            }else if(e.getKey().equals("highest")){
                highestList.add(e.getValue());
            }else if(e.getKey().equals("boom")){
            	boomList.add(e.getValue());
            }
        }
        for(int i : lowestList){
            Player p = GameServer.players.get(i);
            playerScore.put(i,p.getScore()+num*highestList.size());
            p.setScore(p.getScore()+num*highestList.size());
        }
        if(boomList.size()>0) {
        	for(Map.Entry<String,Integer> e:map.entrySet()){
                if(!e.getKey().equals("boom")){
                	Player p = GameServer.players.get(e.getValue());
                	playerScore.put(e.getValue(),p.getScore()+2*boomList.size());
                	p.setScore(p.getScore()+2*boomList.size());
                }
            }
        }
    }


    public static double no2match(Card[] ca){
        double number = 0;
        double sum =0;
        for(Card c:ca){
            if(c.getNumber()>10){
                number = 0.5;
            }else{
                number = c.getNumber();
            }
            sum += number;
        }
        if(sum>10.5){
            sum=0;
        }
        return sum;
    }

    public static double no3match(List<Card> ca){
        Set<Double> set = new HashSet<>();
        List<Double> na = new ArrayList();
        double sum = 0;
        for(Card c:ca){
            set.add(c.getNumber());
            na.add(Card.getAceVlaue(c.getNumber()));
            sum+= Card.getAceVlaue(c.getNumber());
        }
        if(set.size()==1){
            Double[] myArray =  set.toArray(new Double[0]);
            double score = myArray[0];
            return sum*100000;
        }
        boolean sz = isStraightFlush(na);
        boolean th = isSameSuite(ca);
        if(sz&&th){
            return sum*10000;
        }else if(th){
            return sum*1000;
        }else if(sz){
            return sum*100;
        }else if(set.size()==2){
            return sum*10;
        }else if(is235(na)){
            return 0;
        }else{
            Collections.sort(na);
            sum = na.get(2)+na.get(1)*0.1+na.get(0)*0.01;
            return sum;
        }
    }

    //顺子判断
    private static boolean isStraightFlush(List<Double> na) {
        Collections.sort(na);
        for (int i = 0; i < na.size() - 1; i++) {
            double current = Card.getAceVlaue(na.get(i));
            double next = Card.getAceVlaue(na.get(i + 1));
            if (next - current != 1) {
                return false;
            }
        }
        return true;
    }

    // 判断是否为同花
    private static boolean isSameSuite(List<Card> cards) {
        Shape suit = cards.get(0).getShape();
        for (Card card : cards) {
            if (!(card.getShape()==suit)) {
                return false;
            }
        }
        return true;
    }

    //判断235
    private static boolean is235(List<Double> na){
        Collections.sort(na);
        if(na.get(2)!=5){
            return false;
        }
        return true;
    }

    public static List resort(List<Card> list,int[] indices) {
        ArrayList<Card> sortedList = new ArrayList<>();
        for (int index : indices) {
            sortedList.add(list.get(index));
        }
        return sortedList;
    }

}


