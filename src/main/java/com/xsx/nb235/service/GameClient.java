package com.xsx.nb235.service;

import com.xsx.nb235.controller.GameHallController;
import com.xsx.nb235.controller.GameHallInnerController;
import com.xsx.nb235.controller.NavigationManager;
import com.xsx.nb235.controller.WaitingPlayersController;
import com.xsx.nb235.entity.Card;
import com.xsx.nb235.entity.Message;
import com.xsx.nb235.entity.Player;
import com.xsx.nb235.entity.Service;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;

public class GameClient {
    private  Socket socket;

    public static Player self;

    //后期有时间了，改成单例模式
    public static GameClient gc;

    private ObjectOutputStream oos;

    private ObjectInputStream ois ;

    public  boolean receiveFlag = true;
    private static List<Player>List = new ArrayList<>();
    public  boolean joinGame(String ipAddress, int port) {
        try {
            socket = new Socket(ipAddress, port);
            GameServer.local="player";
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

   public void sendMessage(Message message){
        try {
            if(oos==null) {
                oos = new ObjectOutputStream(socket.getOutputStream());
            }
            oos.writeUnshared(message);
            oos.reset();
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void receiveMessages() {
        try {
            if(ois==null){
                ois = new ObjectInputStream(socket.getInputStream());
            }
            while (receiveFlag) {
                Message message = (Message)ois.readObject();
                System.out.println("接收到的数据：" + message.toString());
                handleMessage(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("连接断开");
                alert.setContentText("与服务器的连接已断开！");
                alert.showAndWait();
                NavigationManager.goToIndex();
            });
        }
    }

/*    // 发送心跳包
    public void sendHeartbeat() {
        try {
            while (true) {
                // 每隔一段时间发送心跳包
                writer.println("heartbeat");
                Thread.sleep(5000); // 5秒钟发送一次心跳包
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("连接断开");
                alert.setContentText("与服务器的连接已断开！");
                alert.showAndWait();
            });
        }
    }*/

    // 处理服务器发送的消息
    public  void handleMessage(Message message) {
        switch (message.getService()){
            case PLAYERLIST ->{
                GameServer.players = (ConcurrentSkipListMap<Integer,Player>)message.getObject();
                WaitingPlayersController.playerList.setAll(GameServer.players.values());
                break;
            }
            case YOURSELF -> {
                self = (Player) message.getObject();
                break;
            }
            case SERVERQUIT -> {
                if(socket!=null){
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("连接断开");
                    alert.setContentText("服务器已关闭");
                    alert.showAndWait();
                    receiveFlag = false;
                    NavigationManager.goToIndex();
                });
                break;
            }
            case CLIENTQUIT -> {
                GameServer.players.remove(message.getFrom());
                WaitingPlayersController.playerList.setAll(GameServer.players.values());
            }
            case STARTGAME -> {
                List<Card> list = (List<Card>) message.getObject();
                GameHallController.cardList = list;
                Platform.runLater(() -> {
                    NavigationManager.goToGameHallPage();
                });
            }
            case COMPLETED -> {
                Platform.runLater(() -> {
                    Scene scene = NavigationManager.stage.getScene();
                    Parent currentRoot = scene.getRoot();
                    ImageView iv = (ImageView)currentRoot.lookup("#ok"+message.getFrom());
                    if(iv!=null) {
                    	iv.setVisible(true);
                    }
                    
                });
            }
            case READY -> {
                Platform.runLater(() -> {
                    Scene scene = NavigationManager.stage.getScene();
                    Parent currentRoot = scene.getRoot();
                    ImageView iv = (ImageView)currentRoot.lookup("#ok2"+message.getFrom());
                    if(iv!=null) {
                    	iv.setVisible(true);
                    }
                });
            }
            case GAMEOVER -> {
            	Map map= (Map<String,Map>)message.getObject();
            	GameHallInnerController.lhm = (LinkedHashMap<Integer,List<Card>>)map.get("card");
            	GameHallInnerController.pscore = (LinkedHashMap<Integer,Integer>)map.get("score");
                Platform.runLater(() -> {
                    Scene scene = NavigationManager.stage.getScene();
                    AnchorPane ap =  (AnchorPane)scene.lookup("#paneToReplace");
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/xsx/nb235/controller/gameHall-inner.fxml"));
                    try {
                        Parent root = loader.load();
                        Platform.runLater(() -> {
                            ap.getChildren().setAll(root);
                        });
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
        }
    }
}
