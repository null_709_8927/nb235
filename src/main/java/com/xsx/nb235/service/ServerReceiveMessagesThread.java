package com.xsx.nb235.service;

import com.xsx.nb235.controller.GameHallController;
import com.xsx.nb235.controller.GameHallInnerController;
import com.xsx.nb235.controller.NavigationManager;
import com.xsx.nb235.controller.WaitingPlayersController;
import com.xsx.nb235.entity.Card;
import com.xsx.nb235.entity.Message;
import com.xsx.nb235.entity.Player;
import com.xsx.nb235.entity.Service;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class ServerReceiveMessagesThread extends Task<Void> {

    private Socket socket;

    private Player p ;

    private ObjectInputStream ois;

    private ObjectOutputStream oos;

    public boolean flag = true;

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Player getP() {
        return p;
    }

    public void setP(Player p) {
        this.p = p;
    }

    public ServerReceiveMessagesThread(Socket socket, Player p) {
        this.socket = socket;
        this.p = p;
    }

    @Override
    protected Void call() throws Exception {
        boolean flag = true;
        if(ois==null){
            ois = new ObjectInputStream(socket.getInputStream());
        }
        while(flag) {
            Message message = (Message) ois.readObject();
            handleMessage(new Message(p.getUserID(),0,message.getService(),message.getObject()));
            System.out.println("接收到的数据：" + message.toString());
        }
        return null;
    }


    public  void sendMessage(Message message){
        try {
            if(oos==null) {
                oos = new ObjectOutputStream(socket.getOutputStream());
            }
            oos.writeUnshared(message);
            oos.reset();
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendNotice(Message message){
        ObjectOutputStream objectOutputStream =null;
        for (int key : GameServer.map.keySet()){
            try {
                ServerReceiveMessagesThread srmt = GameServer.map.get(key);
                if(srmt.oos==null){
                    objectOutputStream = new ObjectOutputStream(srmt.getSocket().getOutputStream());
                }else{
                    objectOutputStream = srmt.oos;
                }
                objectOutputStream.writeUnshared(message);
                objectOutputStream.reset();
                objectOutputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public  void handleMessage(Message message) {
        switch (message.getService()){
            case PLAYERLIST ->{
                sendMessage(new Message(Service.YOURSELF,p));
                WaitingPlayersController.playerList.add(p);
                ServerReceiveMessagesThread.sendNotice(new Message(Service.PLAYERLIST,GameServer.players));
            }
            case SERVERQUIT ->{
                ServerReceiveMessagesThread.sendNotice(message);
            }
            case CLIENTQUIT ->{
                ServerReceiveMessagesThread srmt = GameServer.map.get(message.getFrom());
                srmt.flag=false;
                GameServer.map.remove(message.getFrom());
                GameServer.players.remove(message.getFrom());
                WaitingPlayersController.playerList.setAll(GameServer.players.values());
                System.out.println(message.getFrom()+"已退出游戏！");
                srmt.closeSteam();
                ServerReceiveMessagesThread.sendNotice(message);
            }case COMPLETED->{
                Platform.runLater(() -> {
                    Scene scene = NavigationManager.stage.getScene();
                    Parent currentRoot = scene.getRoot();
                    ImageView iv = (ImageView)currentRoot.lookup("#ok"+message.getFrom());
                    iv.setVisible(true);
                });
                PokerGame.playerCardMap.put(message.getFrom(),(int[])message.getObject());
                for(int s:GameServer.map.keySet()) {
                	if(s!=message.getFrom()) {
                        ServerReceiveMessagesThread srmt = GameServer.map.get(s);
                        srmt.sendMessage(new Message(message.getFrom(),0,Service.COMPLETED,null));
                	}
                }
                System.out.println("是这里在调用吗？2");
                GameHallController.readiness.set(GameHallController.readiness.get()+1);
            }case RESTART->{
                Platform.runLater(() -> {
                    Scene scene = NavigationManager.stage.getScene();
                    Parent currentRoot = scene.getRoot();
                    ImageView iv = (ImageView)currentRoot.lookup("#ok2"+message.getFrom());
                    if(iv!=null)iv.setVisible(true);
                });
                for(int s:GameServer.map.keySet()) {
                    if(s!=message.getFrom()) {
                        ServerReceiveMessagesThread srmt = GameServer.map.get(s);
                        srmt.sendMessage(new Message(message.getFrom(),0,Service.READY,null));
                    }
                }
                GameHallInnerController.readiness2.set(GameHallInnerController.readiness2.get()+1);
            }case TIMEOUT -> {
                PokerGame.playerCardMap.put(message.getFrom(),(int[])message.getObject());
                System.out.println("这里居然执行了");
                GameHallController.readiness.set(GameHallController.readiness.get()+1);
            }
        }
    }

    public static void prepareMessage(Message message) {
        switch (message.getService()){
            case STARTGAME ->{
                Map<Integer,List<Card>> map = (LinkedHashMap<Integer,List<Card>>)message.getObject();
                for(int s :GameServer.map.keySet()){
                    ServerReceiveMessagesThread srmt = GameServer.map.get(s);
                    srmt.sendMessage(new Message(Service.STARTGAME,map.get(s)));
                }
            }
        }
    }

    public  void closeSteam(){
        try {
            oos.close();
            ois.close();
            socket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
