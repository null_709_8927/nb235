package com.xsx.nb235.service;

import com.xsx.nb235.controller.WaitingPlayersController;
import com.xsx.nb235.entity.Dog;
import com.xsx.nb235.entity.Player;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class GameServer {
    public static ConcurrentHashMap<Integer,ServerReceiveMessagesThread> map = new ConcurrentHashMap<>();
    public static ConcurrentSkipListMap<Integer,Player> players = new ConcurrentSkipListMap<>();

    public static String local= "";

    public static void creteGame(int port)  {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            local = "host";
            System.out.println("服务器已启动，等待客户端连接...");
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("客户端连接成功！");
                Player p = generateUserInfo(socket);
                players.put(p.getUserID(),p);
                // 创建一个新的线程处理客户端请求
                ServerReceiveMessagesThread grmt = new ServerReceiveMessagesThread(socket,p);
                map.put(p.getUserID(),grmt);
                new Thread(grmt).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Player generateUserInfo (Socket socket){
        InetAddress address = socket.getInetAddress();
        String ipAddress = address.getHostAddress();
        Dog[] dogs = Dog.values();
        Random random = new Random();
        int randomIndex = random.nextInt(dogs.length);
        Dog dog = dogs[randomIndex];
        Player Player = new Player(dog.getName(),ipAddress,GameServer.class.getResource(dog.getPhotoPath()).toExternalForm());
        return  Player;
    }

    public static Player getInfo(){
        InetAddress localHost = null;
        try {
            localHost = InetAddress.getLocalHost();
        } catch (UnknownHostException exception) {
            throw new RuntimeException(exception);
        }
        String ipAddress = localHost.getHostAddress();
        String path = String.valueOf(GameServer.class.getResource("fz.jpg"));
        Player hostPlayer = new Player("主机",ipAddress,path);
        players.put(hostPlayer.getUserID(),hostPlayer);
        return hostPlayer;
    }
}