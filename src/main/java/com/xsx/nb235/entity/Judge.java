package com.xsx.nb235.entity;

public class Judge {

    private boolean whether;

    private double score;

    public Judge(boolean whether, double score) {
        this.whether = whether;
        this.score = score;
    }

    public boolean isWhether() {
        return whether;
    }

    public void setWhether(boolean whether) {
        this.whether = whether;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
