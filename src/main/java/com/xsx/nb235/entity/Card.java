package com.xsx.nb235.entity;

import java.io.Serial;
import java.io.Serializable;

public class Card implements Serializable,Comparable<Card> {
    @Serial
    private static final long serialVersionUID = 5758664381252888875L;
    private double number;
    private Shape shape;
    private String picture;

    public Card(int number, Shape shape, String picture) {
        this.number = number;
        this.shape = shape;
        this.picture = picture;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "Card{" +
                "number=" + number +
                ", shape=" + shape +
                ", picture='" + picture + '\'' +
                '}';
    }

    public static double getAceVlaue(double d){
        if(d==1){
            return 14.0;
        }else{
            return d;
        }
    }

    @Override
    public int compareTo(Card o) {
        if (o.getNumber()==1){
            o.setNumber(14);
        }
        if(this.getNumber()==1){
            this.setNumber(14);
        }
        double r = this.getNumber() - o.getNumber();
        return (int)r;
    }
}
