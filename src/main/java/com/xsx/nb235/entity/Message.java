package com.xsx.nb235.entity;

import java.io.Serial;
import java.io.Serializable;

public class Message implements Serializable {
    @Serial
    private static final long serialVersionUID = -2812396833430341008L;
    private int from;
    private int target;
    private Service service;
    private Object object;
    public Message(int from, int target, Service service, Object object) {
        this.from = from;
        this.target = target;
        this.service = service;
        this.object = object;
    }
    public Message(Service service, Object object) {
        this.service = service;
        this.object = object;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }


    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "Message{" +
                "from='" + from + '\'' +
                ", target='" + target + '\'' +
                ", service=" + service +
                ", object=" + object +
                '}';
    }
}
