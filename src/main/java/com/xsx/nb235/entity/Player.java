package com.xsx.nb235.entity;
import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

public class Player implements Serializable {
    @Serial
    private static final long serialVersionUID = 5851924469283186641L;
    private int userID;
    private String username ;
    private String ipAddress;
    private String avatar;
    private int score = 0;

    private static int incre = 0;

    public Player(String username, String ipAddress, String avatar) {
        this.userID = incre++;
        this.username = username;
        this.ipAddress = ipAddress;
        this.avatar = avatar;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
