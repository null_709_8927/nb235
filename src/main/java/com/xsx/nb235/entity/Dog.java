package com.xsx.nb235.entity;

public enum Dog {
    DHG("大黄狗","dhg.jpg"),HSQ("哈士奇","hsq.jpg"),KJ("柯基","kj.jpg"),TD("泰迪","td.jpg");

    private String name;
    private String photoPath;

    private Dog(String name, String photoPath) {
        this.name = name;
        this.photoPath = photoPath;
    }

    public String getName() {
        return name;
    }

    public String getPhotoPath() {
        return photoPath;
    }

}
