package com.xsx.nb235.entity;

public enum Shape {
    SPADE("black"),HEART("red"),DIAMOND("red"),CLUB("black");

    private String color;

    Shape(String color) {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
