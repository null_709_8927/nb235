package com.xsx.nb235;

import com.xsx.nb235.controller.NavigationManager;
import com.xsx.nb235.entity.Message;
import com.xsx.nb235.entity.Service;
import com.xsx.nb235.service.GameClient;
import com.xsx.nb235.service.GameServer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;

public class Nb235Application extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("index-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("牛B 235");
        stage.setScene(scene);
        stage.show();
        NavigationManager.setStage(stage);

        stage.setOnCloseRequest(event -> {
            event.consume(); // 阻止默认的窗口关闭操作

            // 弹出确认对话框
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("确认关闭");
            alert.setHeaderText("确定要关闭应用吗？");
            alert.setContentText("请确认是否保存数据！");

            // 处理对话框结果
            ButtonType result = alert.showAndWait().orElse(ButtonType.CANCEL);
            if (result == ButtonType.OK) {
                // 用户选择了确定按钮，执行关闭操作
                if(GameServer.local.equals("player")){
                    GameClient.gc.sendMessage(new Message(GameClient.self.getUserID(),0,Service.CLIENTQUIT,GameClient.self));
                }
                stage.close();
                try {
                    // 延时1秒钟，等待消息发送完成
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.exit(0);
                // 可以在这里添加其他相关逻辑
            }
        });
    }

    public static void main(String[] args) {
        launch();
    }
}