package com.xsx.nb235.controller;

import com.xsx.nb235.entity.Card;
import com.xsx.nb235.entity.Message;
import com.xsx.nb235.entity.Player;
import com.xsx.nb235.entity.Service;
import com.xsx.nb235.service.GameClient;
import com.xsx.nb235.service.GameServer;
import com.xsx.nb235.service.PokerGame;
import com.xsx.nb235.service.ServerReceiveMessagesThread;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class WaitingPlayersController implements Initializable {

    @FXML
    private VBox playerListPane;
    @FXML
    private Button startGame;
    public static ObservableList<Player> playerList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        playerList.addListener(new ListChangeListener<Player>() {
            @Override
            public void onChanged(Change<? extends Player> change) {
                while (change.next()) {
                   /* if (change.wasAdded()) {
                        for (Player item : change.getAddedSubList()) {

                        }
                        // 更新UI
                        updateUI();
                    } else if (change.wasRemoved()) {
                        // 处理元素删除的情况
                        for (String item : change.getRemoved()) {
                            System.out.println("删除元素: " + item);
                        }
                        // 更新UI
                        updateUI();
                    }*/
                    Platform.runLater(() -> {
                        NavigationManager.goToWaitingPlayers();
                    });
                }
            }
        });

        if(playerList.size()>0){
            for(Player p:playerList){
                Pane pane = new Pane();
                pane.setStyle("-fx-border-color: #36149d;");
                pane.setPrefHeight(126.0);
                pane.setPrefWidth(329.0);
                ImageView iv = new ImageView();
                iv.setFitHeight(126.0);
                iv.setFitWidth(123.0);
                iv.setImage(new Image(p.getAvatar()));
                iv.setPreserveRatio(false);
                Label username = new Label();
                username.setLayoutX(136.0);
                username.setLayoutY(18.0);
                username.setPrefHeight(40.0);
                username.setPrefWidth(166.0);
                username.setText(p.getUsername());
                Label ipAddress = new Label();
                ipAddress.setLayoutX(136.0);
                ipAddress.setLayoutY(68.0);
                ipAddress.setPrefHeight(40.0);
                ipAddress.setPrefWidth(166.0);
                ipAddress.setText(p.getUsername());
                ipAddress.setText(p.getIpAddress());
                Button button = new Button();
                button.setLayoutX(481.0);
                button.setLayoutY(33.0);
                button.setPrefHeight(50.0);
                button.setPrefWidth(145.0);
                button.setVisible(false);
                button.setText("准备");
                pane.getChildren().add(iv);
                pane.getChildren().add(username);
                pane.getChildren().add(ipAddress);
                pane.getChildren().add(button);
                playerListPane.getChildren().add(pane);
            }
        }
        startGame.setOnAction(e->{
            if(GameServer.local.equals("host")){
                Map<Integer, List<Card>> cardsMap = PokerGame.dealCards (GameServer.players);
                GameHallController.cardList = cardsMap.get(GameClient.self.getUserID());
                ServerReceiveMessagesThread.prepareMessage(new Message(Service.STARTGAME,cardsMap));
                NavigationManager.goToGameHallPage();
            }else{
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("权限不足");
                    alert.setContentText("你不是房主！");
                    alert.showAndWait();
                });
            }
        });
    }

}
