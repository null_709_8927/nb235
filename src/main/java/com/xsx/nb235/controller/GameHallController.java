package com.xsx.nb235.controller;

import com.xsx.nb235.entity.Card;
import com.xsx.nb235.entity.Message;
import com.xsx.nb235.entity.Player;
import com.xsx.nb235.entity.Service;
import com.xsx.nb235.service.GameClient;
import com.xsx.nb235.service.GameServer;
import com.xsx.nb235.service.PokerGame;
import com.xsx.nb235.service.ServerReceiveMessagesThread;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class GameHallController implements Initializable {

    @FXML
    private HBox playerScore;

    private ImageView selectedPoker;

    private ImageView tagetPoker;

    @FXML
    private ImageView cardno1;

    @FXML
    private ImageView cardno2;

    @FXML
    private ImageView cardno3;

    @FXML
    private ImageView cardno4;

    @FXML
    private ImageView cardno5;

    @FXML
    private ImageView cardno6;

    @FXML
    private Button finished;
    @FXML
    private Label second;

    private int countdownSeconds = 60;
    
    public static ReadOnlyIntegerWrapper readiness = null;

    Timeline timeline =null;

    //public static ObservableList<Player> playerList = FXCollections.observableArrayList(GameServer.players.values());
    private List<Player> playerList = new ArrayList();
    public static List<Card> cardList = new ArrayList<>();
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    	System.out.println("加载gamehell");
    	readiness = new ReadOnlyIntegerWrapper(0);
    	playerList.addAll(GameServer.players.values());
        for(Player p:playerList){
            StackPane pane = new StackPane();
            pane.setPrefHeight(90.0);
            pane.setPrefWidth(113.0);
            ImageView iv = new ImageView();
            iv.setFitHeight(102.0);
            iv.setFitWidth(112.0);
            iv.setImage(new Image(p.getAvatar()));
            iv.setPreserveRatio(false);
            Label score = new Label();
            score.setId("score"+p.getUserID());
            score.setAlignment(Pos.CENTER);
            score.setContentDisplay(ContentDisplay.CENTER);
            score.setPrefHeight(29);
            score.setPrefWidth(113);
            score.setText(String.valueOf(p.getScore()));
            ImageView iv2 = new ImageView();
            iv2.setId("ok"+p.getUserID());
            iv2.setFitWidth(51.0);
            iv2.setFitWidth(58.0);
            iv2.setImage(new Image(getClass().getResource("ok.png").toExternalForm()));
            iv2.setPreserveRatio(true);
            iv2.setVisible(false);
            pane.getChildren().add(iv);
            pane.getChildren().add(score);
            pane.getChildren().add(iv2);
            pane.setAlignment(iv,Pos.TOP_CENTER);
            pane.setAlignment(score,Pos.BOTTOM_CENTER);
            pane.setAlignment(iv2,Pos.CENTER_RIGHT);
            playerScore.getChildren().add(pane);
        }
        cardno1.setImage(new Image(cardList.get(0).getPicture()));
        cardno1.setUserData(0);
        setDragDropHandler(cardno1);
        cardno2.setImage(new Image(cardList.get(1).getPicture()));
        cardno2.setUserData(1);
        setDragDropHandler(cardno2);
        cardno3.setImage(new Image(cardList.get(2).getPicture()));
        cardno3.setUserData(2);
        setDragDropHandler(cardno3);
        cardno4.setImage(new Image(cardList.get(3).getPicture()));
        cardno4.setUserData(3);
        setDragDropHandler(cardno4);
        cardno5.setImage(new Image(cardList.get(4).getPicture()));
        cardno5.setUserData(4);
        setDragDropHandler(cardno5);
        cardno6.setImage(new Image(cardList.get(5).getPicture()));
        cardno6.setUserData(5);
        setDragDropHandler(cardno6);

        finished.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handleButtonClick(event);
            }
        });

        timeline = new Timeline(
            new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                	if (!timeline.getStatus().equals(Animation.Status.RUNNING)) {
                        return; 
                    }
                    countdownSeconds--;
                    second.setText(String.valueOf(countdownSeconds) );
                    if (countdownSeconds == 0) {
                        timeline.stop();
                        if(GameServer.local.equals("host")){
                            PokerGame.playerCardMap.put(GameClient.self.getUserID(),getMyPokerSequence());
                            System.out.println("time line 调用了");
                            readiness.set(readiness.get()+1);
                        }else{
                            GameClient.gc.sendMessage(new Message(GameClient.self.getUserID(),0, Service.TIMEOUT,getMyPokerSequence()));
                        }
                    }
                }
            })
        );
        timeline.setCycleCount(countdownSeconds);
        timeline.play();
        
        ReadOnlyIntegerProperty readOnlyIntegerProperty = readiness.getReadOnlyProperty();
        readOnlyIntegerProperty.addListener((observable, oldValue, newValue) -> {
            System.out.println("监听器新"+newValue.intValue());
            System.out.println("监听器旧"+oldValue.intValue());
            if (newValue.intValue() == playerList.size()) {
                System.out.println("监听器在执行"+PokerGame.playerCardMap.size());
                Map[] Score =PokerGame.judge();
                PokerGame.statisticalScore(PokerGame.CalculateRanking(Score[0]),2);
                PokerGame.statisticalScore(PokerGame.CalculateRanking(Score[1]),2);
                PokerGame.statisticalScore(PokerGame.CalculateRanking(Score[2]),5);
                Map<String,Map> hashMap = new HashMap<>();
                hashMap.put("card",PokerGame.cardsMap);
                hashMap.put("score",PokerGame.playerScore);
                GameHallInnerController.lhm = PokerGame.cardsMap;
                GameHallInnerController.pscore = PokerGame.playerScore;
                ServerReceiveMessagesThread.sendNotice(new Message(0,0,Service.GAMEOVER,hashMap));

                Platform.runLater(() -> {
                    Scene scene = NavigationManager.stage.getScene();
                    AnchorPane ap =  (AnchorPane)scene.lookup("#paneToReplace");
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("gameHall-inner.fxml"));
                    Parent root = null;
                    try {
                        root = loader.load();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    ap.getChildren().setAll(root);
                });
            }
        });
    }

    public int[] getMyPokerSequence(){
        int[] sequence = {(int)cardno1.getUserData(),(int)cardno2.getUserData(),(int)cardno3.getUserData(),(int)cardno4.getUserData(),(int)cardno5.getUserData(),(int)cardno6.getUserData()};
        return sequence;
    }

    private void handleButtonClick(ActionEvent event) {
        timeline.stop();
        ImageView iv=(ImageView)playerScore.lookup("#ok"+GameClient.self.getUserID());
        iv.setVisible(true);
        finished.setDisable(true);
        unbind(cardno1);
        unbind(cardno2);
        unbind(cardno3);
        unbind(cardno4);
        unbind(cardno5);
        unbind(cardno6);
        if(GameServer.local.equals("host")){
            PokerGame.playerCardMap.put(GameClient.self.getUserID(),getMyPokerSequence());
            ServerReceiveMessagesThread.sendNotice(new Message(GameClient.self.getUserID(),0,Service.COMPLETED,null));
            readiness.set(readiness.get()+1);
        }else{
            GameClient.gc.sendMessage(new Message(GameClient.self.getUserID(),0, Service.COMPLETED,getMyPokerSequence()));
        }
    }

    private void setDragDropHandler(ImageView imageView){

        imageView.setOnDragDetected(event -> {
            // 启动拖拽操作，并传递相关数据
            imageView.startDragAndDrop(TransferMode.MOVE);
            imageView.setOpacity(0.5); // 设置ImageView的透明度

            // 将源ImageView的图像作为拖拽目标传递
            ClipboardContent content = new ClipboardContent();
            content.putImage(imageView.getImage());
            imageView.startDragAndDrop(TransferMode.MOVE).setContent(content);

            event.consume();
        });

        imageView.setOnDragOver(event -> {
            // 指定在拖拽目标上放置时允许的操作类型
            if (event.getGestureSource() != imageView && event.getDragboard().hasImage()) {
                event.acceptTransferModes(TransferMode.MOVE);
            }
            event.consume();
        });

        imageView.setOnDragDropped(event -> {
            // 获取拖拽数据中的图像
            Image draggedImage = event.getDragboard().getImage();

            // 交换源ImageView和目标ImageView的图像
            selectedPoker = (ImageView) event.getGestureSource();
            tagetPoker =  (ImageView) event.getPickResult().getIntersectedNode();
            int i = (Integer) selectedPoker.getUserData();
            int t = (Integer) tagetPoker.getUserData();
            selectedPoker.setUserData(t);
            tagetPoker.setUserData(i);
            //Collections.swap(cardList, i, t);   集合不动了，让顺序改变。
            Image tagetImage = tagetPoker.getImage();
            tagetPoker.setImage(draggedImage);
            selectedPoker.setImage(tagetImage);
            selectedPoker.setOpacity(1);

            event.setDropCompleted(true);
            event.consume();
        });
    }

    private void unbind(ImageView imageView) {
        imageView.setOnDragDetected(null);
        imageView.setOnDragOver(null);
        imageView.setOnDragDropped(null);
    }
}
