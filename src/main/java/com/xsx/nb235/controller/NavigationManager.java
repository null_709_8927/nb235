package com.xsx.nb235.controller;

import com.xsx.nb235.service.GameServer;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class NavigationManager {

    public static Stage stage;

    public static Stage connectStage;

    public static void setStage(Stage stage) {
        NavigationManager.stage = stage;
    }

    public static void goToIndex() {
        navigateTo("index-view.fxml");
    }

    public static void goToWaitingPlayers() {
        navigateTo("waitingPlayers.fxml");
    }

    public static void goToConnectPage() {
        navigateToNewStage("connect-view.fxml");
    }

    public static void goToGameHallPage(){
        navigateTo("gameHall.fxml");
    }

    private static void navigateTo(String fxmlFile) {
        try {
            Parent root = FXMLLoader.load(NavigationManager.class.getResource(fxmlFile));
            stage.setScene(new Scene(root));
            if(GameServer.local.equals("host")){
                stage.setTitle("nb235  主机");
            }
           // stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void navigateToNewStage(String fxmlFile) {
        try {
            Parent root = FXMLLoader.load(NavigationManager.class.getResource(fxmlFile));
            if(connectStage==null)connectStage = new Stage();
            connectStage.setResizable(false);
            connectStage.setScene(new Scene(root));
            connectStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void loadAndReplace(String fxmlFile) {
        try {
            FXMLLoader loader = new FXMLLoader(NavigationManager.class.getResource("gameHall-inner.fxml"));
            Parent newRoot = loader.load();

            Platform.runLater(() -> {
                Scene scene = stage.getScene();
                Parent currentRoot = scene.getRoot();
                AnchorPane paneToReplace = (AnchorPane)currentRoot.lookup("#paneToReplace");
                paneToReplace.getChildren().setAll(newRoot);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}