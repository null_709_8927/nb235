package com.xsx.nb235.controller;

import com.xsx.nb235.entity.Player;
import com.xsx.nb235.service.GameClient;
import com.xsx.nb235.service.GameServer;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class IndexController implements Initializable {
    @FXML
    private Button createGame;

    @FXML
    private Button joinGame;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        createGame.setOnAction(e->{
            GameServer.players.clear();
            Player host = GameServer.getInfo();
            GameClient.self = host;
            Task<Void> serverTask = new Task<Void>(){
                @Override
                protected Void call() throws Exception {
                    GameServer.creteGame(8235);
                    return null;
                }
            };
            Thread gameThread = new Thread(serverTask);
            gameThread.start();
            WaitingPlayersController.playerList.setAll(GameServer.players.values());
            NavigationManager.goToWaitingPlayers();
        });
        joinGame.setOnAction(e->{
            NavigationManager.goToConnectPage();
        });


    }
}