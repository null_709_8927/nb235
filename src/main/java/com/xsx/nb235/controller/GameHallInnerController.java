package com.xsx.nb235.controller;

import com.xsx.nb235.entity.Card;
import com.xsx.nb235.entity.Message;
import com.xsx.nb235.entity.Player;
import com.xsx.nb235.entity.Service;
import com.xsx.nb235.service.GameClient;
import com.xsx.nb235.service.GameServer;
import com.xsx.nb235.service.PokerGame;
import com.xsx.nb235.service.ServerReceiveMessagesThread;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class GameHallInnerController implements Initializable {

    @FXML
    private HBox playerScore;
    public static Map<Integer,List<Card>> lhm = new LinkedHashMap();

    public static Map<Integer,Integer> pscore = new LinkedHashMap();

    public static ReadOnlyIntegerWrapper readiness2 = null;

    @FXML
    private VBox scorePane;

    @FXML
    private Button continueButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    	readiness2 = new ReadOnlyIntegerWrapper(0);
    	System.out.println("innerController");
        for(Player p:GameServer.players.values()){
            StackPane pane = new StackPane();
            pane.setPrefHeight(90.0);
            pane.setPrefWidth(113.0);
            ImageView iv = new ImageView();
            iv.setFitHeight(102.0);
            iv.setFitWidth(112.0);
            iv.setImage(new Image(p.getAvatar()));
            iv.setPreserveRatio(false);
            Label score = new Label();
            score.setId("score"+p.getUserID());
            score.setAlignment(Pos.CENTER);
            score.setContentDisplay(ContentDisplay.CENTER);
            score.setPrefHeight(29);
            score.setPrefWidth(113);
            if(pscore.get(p.getUserID())==null){
                score.setText("0");
            }else{
                p.setScore(pscore.get(p.getUserID()));
                score.setText(String.valueOf(pscore.get(p.getUserID())));
            }
            ImageView iv2 = new ImageView();
            iv2.setId("ok2"+p.getUserID());
            iv2.setFitWidth(51.0);
            iv2.setFitWidth(58.0);
            iv2.setImage(new Image(getClass().getResource("ok.png").toExternalForm()));
            iv2.setPreserveRatio(true);
            iv2.setVisible(false);
            pane.getChildren().add(iv);
            pane.getChildren().add(score);
            pane.getChildren().add(iv2);
            pane.setAlignment(iv,Pos.TOP_CENTER);
            pane.setAlignment(score,Pos.BOTTOM_CENTER);
            pane.setAlignment(iv2,Pos.CENTER_RIGHT);
            playerScore.getChildren().add(pane);
        }
        for(int s:lhm.keySet()){
            if(s== GameClient.self.getUserID()){
                continue;
            }
            ImageView iv = new ImageView();
            iv.setFitHeight(100.0);
            iv.setFitWidth(100.0);
            iv.setImage(new Image(GameServer.players.get(s).getAvatar()));
            iv.setPreserveRatio(false);

            ImageView c1 = new ImageView();
            c1.setFitHeight(100.0);
            c1.setFitWidth(70.0);
            c1.setImage(new Image(lhm.get(s).get(0).getPicture()));
            c1.setPreserveRatio(false);

            Line l1 = new Line();
            l1.setEndX(-99.70710754394531);
            l1.setEndY(99.29289245605469);
            l1.setStartX(-100.0);

            ImageView c2 = new ImageView();
            c2.setFitHeight(100.0);
            c2.setFitWidth(70.0);
            c2.setImage(new Image(lhm.get(s).get(1).getPicture()));
            c2.setPreserveRatio(false);

            ImageView c3 = new ImageView();
            c3.setFitHeight(100.0);
            c3.setFitWidth(70.0);
            c3.setImage(new Image(lhm.get(s).get(2).getPicture()));
            c3.setPreserveRatio(false);

            Line l2 = new Line();
            l2.setEndX(-100.70710754394531);
            l2.setEndY(99.29289245605469);
            l2.setStartX(-100.0);

            ImageView c4 = new ImageView();
            c4.setFitHeight(100.0);
            c4.setFitWidth(70.0);
            c4.setImage(new Image(lhm.get(s).get(3).getPicture()));
            c4.setPreserveRatio(false);

            ImageView c5 = new ImageView();
            c5.setFitHeight(100.0);
            c5.setFitWidth(70.0);
            c5.setImage(new Image(lhm.get(s).get(4).getPicture()));
            c5.setPreserveRatio(false);

            ImageView c6 = new ImageView();
            c6.setFitHeight(100.0);
            c6.setFitWidth(70.0);
            c6.setImage(new Image(lhm.get(s).get(5).getPicture()));

            HBox box = new HBox();
            box.setPrefHeight(100);
            box.setPrefWidth(810);

            box.getChildren().add(iv);
            Insets insets = new Insets(0, 0, 0, 30.0);
            box.getChildren().add(c1);
            HBox.setMargin(c1,insets);
            box.getChildren().add(l1);

            box.getChildren().add(c2);
            box.getChildren().add(c3);

            box.getChildren().add(l2);
            box.getChildren().add(c4);
            box.getChildren().add(c5);
            box.getChildren().add(c6);
            scorePane.getChildren().add(box);
        }
        restart(continueButton);

        ReadOnlyIntegerProperty readOnlyIntegerProperty = readiness2.getReadOnlyProperty();
        readOnlyIntegerProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() == GameServer.players.size()) {
                Map<Integer, List<Card>> cardsMap = PokerGame.dealCards (GameServer.players);
                GameHallController.cardList = cardsMap.get(GameClient.self.getUserID());
                ServerReceiveMessagesThread.prepareMessage(new Message(Service.STARTGAME,cardsMap));
                Platform.runLater(() -> {
                    NavigationManager.goToGameHallPage();
                });
            }
        });
    }

    private void restart(Button button){
        button.setOnAction(e->{
            ImageView iv=(ImageView)playerScore.lookup("#ok2"+GameClient.self.getUserID());
            iv.setVisible(true);
            continueButton.setDisable(true);
            if(GameServer.local.equals("host")){
                ServerReceiveMessagesThread.sendNotice(new Message(GameClient.self.getUserID(),0,Service.READY,null));
                readiness2.set(readiness2.get()+1);
            }else{
                GameClient.gc.sendMessage(new Message(GameClient.self.getUserID(),0, Service.RESTART,null));
            }
        });
    }
}
