package com.xsx.nb235.controller;

import com.xsx.nb235.entity.Message;
import com.xsx.nb235.entity.Service;
import com.xsx.nb235.service.GameClient;
import com.xsx.nb235.service.GameServer;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class ConnectController implements Initializable {

    @FXML
    private Button connect;

    @FXML
    private TextField ipAddress;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

         connect.setOnAction(e->{
                String ip = ipAddress.getText();
                //作为客户端 加入游戏
                connect.setDisable(true);
                GameClient gc= new GameClient();
                GameClient.gc = gc;
            Task<Boolean> clientTask = new Task<>() {
                @Override
                protected Boolean call() throws Exception {
                    return gc.joinGame(ip,8235);
                }
            };
            clientTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent event) {
                    Boolean result = clientTask.getValue();
                    if(result){
                        new Thread(new Task<Void>() {
                                @Override
                            protected Void call() throws Exception {
                                gc.receiveMessages();
                                return null;
                            }
                        }).start();
                        gc.sendMessage(new Message(Service.PLAYERLIST,null));
                        NavigationManager.connectStage.close();
                        NavigationManager.goToWaitingPlayers();
                    }else{
                        Platform.runLater(() -> {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("连接失败");
                            alert.setContentText("无法连接到服务器！");
                            alert.showAndWait();
                            connect.setDisable(false);
                        });
                    }
                }
            });
            Thread gameThread = new Thread(clientTask);
            gameThread.start();
        });
    }
}
